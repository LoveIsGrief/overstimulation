# Requirements

 * [Virtualbox](https://www.virtualbox.org/wiki/Downloads) the virtual machine runner
 * [Vagrant](https://www.vagrantup.com/downloads.html) for downloading and provisioning the VM

# Starting the VM
 
```bash
# Once you're in the directory
# Startup the VM
vagrant up
```

# What next

Login with the password `vagrant`.

You can now launch the script with the shortcut on the desktop "Overstimulation".
