#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "waiting for dpkg lock"
sleep 5

apt-get update
apt-get -y -qq install python python-pip python-opencv python-numpy \
    libavformat-dev \
    libavcodec-dev \
    libavdevice-dev \
    libavutil-dev \
    libavfilter-dev \
    libswscale-dev \
    libavresample-dev \
    x11-xserver-utils \
    vlc ffmpeg \
    vim nano \
    pkg-config

pip install av psutil

# Add launcher to project to desktop
OVER_DIR=/home/vagrant/Overstimulation
DESKTOP_DIR=/home/vagrant/Desktop
mkdir -p $OVER_DIR $DESKTOP_DIR
chown vagrant:vagrant $OVER_DIR
chown vagrant:vagrant $DESKTOP_DIR
ln -nfs /vagrant/Overstimulation.desktop $DESKTOP_DIR
