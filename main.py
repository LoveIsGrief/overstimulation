"""

"""
import time

import psutil

__author__ = "LoveIsGrief"

import av
import os


def get_screen_resolution():
    import subprocess
    return subprocess.Popen('xrandr | grep "\*" | cut -d" " -f4', shell=True, stdout=subprocess.PIPE).communicate()[
        0].strip()


SCREEN_RESOLUTION = get_screen_resolution()
DISPLAY = os.environ["DISPLAY"]
FRAME_RATE = 25
print "DISPLAY: %s" % DISPLAY
print "SCREEN_RESOLUTION: %s" % SCREEN_RESOLUTION
print "FRAME_RATE: %s" % FRAME_RATE

# ffmpeg -video_size 1024x768 -framerate 25 -f x11grab -i :0.0+100,200 output.mp4
# Setup screen recording
in_container = av.open(file=DISPLAY, format="x11grab", options={
    "framerate": str(FRAME_RATE),
    "video_size": SCREEN_RESOLUTION
})

# Setup output
filename = "%s.mp4" % (time.time())
filepath = os.path.join(os.path.abspath(os.path.curdir), filename)

out_container = av.open(filepath, "w")
output_stream = out_container.add_stream("mpeg4", FRAME_RATE)
# TODO decide on the quality

frame_count = 0
video_streams = [stream for stream in in_container.streams if stream.type == "video"]

for stream in video_streams:
    print("video stream", stream.format)

# Start screen recording
for packet in in_container.demux(video_streams):
    print "CPU usage: %s" % (psutil.cpu_percent(),)
    print "RAM usage: %s" % (psutil.virtual_memory(),)
    for frame in packet.decode():
        if packet.stream.type == 'video':
            frame_count += 1
            print("frame", frame_count)
        else:
            continue

        # Let av calculate when to present the frame
        frame.pts = None

        output_packets = [output_stream.encode(frame)]
        while output_packets[-1]:
            output_packets.append(output_stream.encode(None))

        for p in output_packets:
            if p:
                out_container.mux(p)

    # Stop writing after 10 seconds
    if frame_count >= 250:
        break

out_container.close()

print "Saved file %s" % filepath

input("Hit Enter to quit")
